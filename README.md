# vhdl-processor

This project was developed in the study module IC design.
The goal of the module was to gain experience with VHDL and Xilinx FPGAs.
During the project we had to develop a processor according to the specifications of our lecturer. 
In parallel we could test and evaluate our programs on a Xilinx Zedboard.

In this repository the solutions of the given tasks (lab tasks) were collected.
