LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY neq4_test IS
END ENTITY;

ARCHITECTURE behaviour OF neq4_test IS
    SIGNAL s_a, s_b : bit_vector(3 DOWNTO 0);
    SIGNAL s_y : BIT;
BEGIN
    dut : ENTITY work.neq4(dataflow)
        PORT MAP(
            A => s_a,
            B => s_b,
            Y => s_y);

    proc_stim : PROCESS BEGIN
        -- expect: s_y = 0
        s_a <= (OTHERS => '0');
        s_b <= (OTHERS => '0');
        WAIT FOR 10 ns;

        -- expect: s_y = 0
        s_a <= (OTHERS => '1');
        s_b <= (OTHERS => '1');
        WAIT FOR 10 ns;

        -- expect: s_y = 1
        s_a <= (3 | 1 => '1', OTHERS => '0');
        s_b <= (2 | 0 => '1', OTHERS => '0');
        WAIT FOR 10 ns;

        -- expect: s_y = 1
        s_a <= (2 | 0 => '1', OTHERS => '0');
        s_b <= (3 | 1 => '1', OTHERS => '0');
        WAIT FOR 10 ns;

        -- expect: s_y = 1
        s_a <= (3 | 2 => '1', OTHERS => '0');
        s_b <= (1 | 0 => '1', OTHERS => '0');
        WAIT FOR 10 ns;
    END PROCESS;
END behaviour;