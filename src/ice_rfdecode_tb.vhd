LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_rfdecode_tb IS
END ice_rfdecode_tb;

ARCHITECTURE behaviour OF ice_rfdecode_tb IS
    SIGNAL s_addr : STD_LOGIC_VECTOR(3 DOWNTO 0);
    SIGNAL s_en : STD_LOGIC;
    SIGNAL s_sel : STD_LOGIC_VECTOR(15 DOWNTO 0);
BEGIN
    dut : ENTITY work.ice_rfdecode
        PORT MAP(
            addr => s_addr,
            en => s_en,
            sel => s_sel);

    s_en <= '0',
        '1' AFTER 16 ns;

    s_addr <= "1111",
        "1110" AFTER 1 ns,
        "1101" AFTER 2 ns,
        "1100" AFTER 3 ns,
        "1011" AFTER 4 ns,
        "1010" AFTER 5 ns,
        "1001" AFTER 6 ns,
        "1000" AFTER 7 ns,
        "0111" AFTER 8 ns,
        "0110" AFTER 9 ns,
        "0101" AFTER 10 ns,
        "0100" AFTER 11 ns,
        "0011" AFTER 12 ns,
        "0010" AFTER 13 ns,
        "0001" AFTER 14 ns,
        "0000" AFTER 15 ns,
        "1111" AFTER 16 ns,
        "1110" AFTER 17 ns,
        "1101" AFTER 18 ns,
        "1100" AFTER 19 ns,
        "1011" AFTER 20 ns,
        "1010" AFTER 21 ns,
        "1001" AFTER 22 ns,
        "1000" AFTER 23 ns,
        "0111" AFTER 24 ns,
        "0110" AFTER 25 ns,
        "0101" AFTER 26 ns,
        "0100" AFTER 27 ns,
        "0011" AFTER 28 ns,
        "0010" AFTER 29 ns,
        "0001" AFTER 30 ns,
        "0000" AFTER 31 ns;
END behaviour;