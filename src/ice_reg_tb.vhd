LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_reg_tb IS
    GENERIC (width : POSITIVE := 16);
END ice_reg_tb;

ARCHITECTURE behaviour OF ice_reg_tb IS
    SIGNAL s_clk, s_reset, s_en : STD_LOGIC;
    SIGNAL s_d : STD_LOGIC_VECTOR(width - 1 DOWNTO 0);
    SIGNAL s_q : STD_LOGIC_VECTOR(width - 1 DOWNTO 0);
BEGIN
    dut : ENTITY work.ice_reg
        PORT MAP(
            clk => s_clk,
            reset => s_reset,
            en => s_en,
            d => s_d,
            q => s_q);
    PROCESS
    BEGIN
        s_en <= '0';
        s_d <= "1001100110011001";
        s_reset <= '1';
        WAIT FOR 1 ns;

        s_en <= '1';
        s_clk <= '0';
        WAIT FOR 1 ns;

        s_reset <= '0';
        s_clk <= '0';
        WAIT FOR 1 ns;

        s_reset <= '0';
        s_clk <= '1';
        WAIT FOR 1 ns;

        s_reset <= '0';
        s_clk <= '1';
        WAIT FOR 1 ns;

        s_clk <= '0';
        s_en <= '0';
        WAIT FOR 0.5 ns;

        s_reset <= '0';
        s_clk <= '1';
        s_d <= "0011001100110011";
        WAIT FOR 1 ns;

        s_en <= '1';
        WAIT FOR 0.5 ns;

        s_reset <= '0';
        s_clk <= '0';
        s_d <= "0011001100110011";
        WAIT FOR 1 ns;

        s_reset <= '0';
        s_clk <= '1';
        s_d <= "0011001100110011";
        WAIT FOR 1 ns;

        s_reset <= '0';
        s_clk <= '1';
        s_d <= "0011001100110011";
        WAIT FOR 1 ns;
    END PROCESS;
END behaviour;