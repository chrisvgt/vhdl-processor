LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_pc_tb IS
END ice_pc_tb;

ARCHITECTURE behaviour OF ice_pc_tb IS
    SIGNAL s_clk, s_reset, s_inc, s_load : STD_LOGIC;
    SIGNAL s_imm, s_pc : STD_LOGIC_VECTOR(11 DOWNTO 0);
BEGIN
    dut : ENTITY work.ice_pc
        PORT MAP(
            clk => s_clk,
            reset => s_reset,
            inc => s_inc,
            load => s_load,
            imm => s_imm,
            pc => s_pc
        );

    PROCESS
    BEGIN
        s_reset <= '0';
        s_clk <= '0';
        s_inc <= '1';
        s_load <= '0';
        s_imm <= "111111111110";
        WAIT FOR 1 ns;

        s_clk <= '1';
        WAIT FOR 1 ns;

        s_clk <= '0';
        WAIT FOR 1 ns;

        s_clk <= '1';
        s_reset <= '1';-- Reset, expect pc <= (others => 0)
        WAIT FOR 1 ns;

        -- increase, expect pc = 0x001
        s_reset <= '0';
        s_clk <= '0';
        WAIT FOR 1 ns;

        s_clk <= '1';
        WAIT FOR 1 ns;

        -- dont increase, expect pc = 0x001
        s_clk <= '0';
        s_inc <= '0';
        WAIT FOR 1 ns;

        s_clk <= '1';
        WAIT FOR 1 ns;

        -- expect load: pc = 0xFF0
        s_clk <= '0';
        s_inc <= '1';
        s_load <= '1';
        s_imm <= "111111110000";
        WAIT FOR 1 ns;

        s_clk <= '1';
        WAIT FOR 1 ns;

        -- expect further increase: pc = 0xFF1
        s_clk <= '0';
        s_load <= '0';
        WAIT FOR 1 ns;

        s_clk <= '1';
        WAIT FOR 1 ns;
    END PROCESS;

END behaviour;