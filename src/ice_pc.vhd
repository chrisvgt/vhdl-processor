LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ice_pc IS
    PORT (
        clk, reset, inc, load : IN STD_LOGIC;
        imm : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        pc : OUT STD_LOGIC_VECTOR(11 DOWNTO 0));
END ice_pc;

ARCHITECTURE behaviour OF ice_pc IS
    SIGNAL pc_next : STD_LOGIC_VECTOR(11 DOWNTO 0);
BEGIN
    PROCESS (clk, reset)
    BEGIN
        IF reset = '1' THEN
            pc <= (OTHERS => '0');
        ELSIF clk 'event AND clk = '1' THEN
            IF inc = '1' THEN
                IF load = '1' THEN
                    pc_next <= imm;
                ELSE
                    pc_next <= STD_LOGIC_VECTOR(unsigned(pc_next) + 1);
                END IF;
            END IF;
        END IF;
        pc <= pc_next;
    END PROCESS;
END behaviour;