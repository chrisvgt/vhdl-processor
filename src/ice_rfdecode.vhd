LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_rfdecode IS
    PORT (
        addr : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        en : IN STD_LOGIC;
        sel : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
END ice_rfdecode;

ARCHITECTURE behaviour OF ice_rfdecode IS

BEGIN
    sel <= (OTHERS => '0') WHEN en = '0' ELSE
        (0 => '1', OTHERS => '0') WHEN addr = "0000" ELSE
        (1 => '1', OTHERS => '0') WHEN addr = "0001" ELSE
        (2 => '1', OTHERS => '0') WHEN addr = "0010" ELSE
        (3 => '1', OTHERS => '0') WHEN addr = "0011" ELSE
        (4 => '1', OTHERS => '0') WHEN addr = "0100" ELSE
        (5 => '1', OTHERS => '0') WHEN addr = "0101" ELSE
        (6 => '1', OTHERS => '0') WHEN addr = "0110" ELSE
        (7 => '1', OTHERS => '0') WHEN addr = "0111" ELSE
        (8 => '1', OTHERS => '0') WHEN addr = "1000" ELSE
        (9 => '1', OTHERS => '0') WHEN addr = "1001" ELSE
        (10 => '1', OTHERS => '0') WHEN addr = "1010" ELSE
        (11 => '1', OTHERS => '0') WHEN addr = "1011" ELSE
        (12 => '1', OTHERS => '0') WHEN addr = "1100" ELSE
        (13 => '1', OTHERS => '0') WHEN addr = "1101" ELSE
        (14 => '1', OTHERS => '0') WHEN addr = "1110" ELSE
        (15 => '1', OTHERS => '0') WHEN addr = "1111";
END behaviour;