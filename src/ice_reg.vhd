LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_reg IS
    GENERIC (width : POSITIVE := 16);
    PORT (
        clk, reset, en : IN STD_LOGIC;
        d : IN STD_LOGIC_VECTOR(width - 1 DOWNTO 0);
        q : OUT STD_LOGIC_VECTOR(width - 1 DOWNTO 0));
END ice_reg;

ARCHITECTURE behaviour OF ice_reg IS

BEGIN
    PROCESS (clk)
    BEGIN
        IF reset = '1' THEN
            q <= (OTHERS => '0');
        ELSIF clk ' event AND clk = '1' THEN
            IF en = '1' THEN
                q <= d;
            END IF;
        END IF;
    END PROCESS;
END behaviour;