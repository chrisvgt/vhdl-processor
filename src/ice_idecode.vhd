LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_idecode IS
    PORT (
        imm : IN STD_LOGIC;
        pc_inc, pc_load : OUT STD_LOGIC;
        grp : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
        opc : IN STD_LOGIC_VECTOR(3 DOWNTO 0));
END ice_idecode;

ARCHITECTURE behaviour OF ice_idecode IS

BEGIN
    PROCESS (imm, grp, opc)
    BEGIN
        -- default parameter
        pc_inc <= '1'; -- Befehlszähler ist aktiviert
        pc_load <= '0'; -- keine Sprungadresse in den Befehlszähler geladen

        IF (opc = "0000") AND (grp = "000") THEN
            pc_inc <= '1';
            pc_load <= '1'; -- Sprungadresse wird in den Befehlszähler geladen
        ELSE

        END IF;
    END PROCESS;
END behaviour;