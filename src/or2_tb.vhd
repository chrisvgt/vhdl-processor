LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY or2_tb IS
END or2_tb;

ARCHITECTURE behaviour OF or2_tb IS
    SIGNAL A, B, Y : STD_LOGIC;
BEGIN
    dut : ENTITY work.or2
        PORT MAP(
            a => A,
            b => B,
            y => Y);
    A <= '1',
        '0' AFTER 15 ns,
        '1' AFTER 35 ns,
        '0' AFTER 45 ns,
        '1' AFTER 65 ns;
    B <= '1',
        '0' AFTER 10 ns,
        '1' AFTER 25 ns,
        '0' AFTER 35 ns,
        '1' AFTER 55 ns,
        '0' AFTER 70 ns;
END behaviour;