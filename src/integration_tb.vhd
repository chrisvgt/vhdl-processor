LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY integration_tb IS
END integration_tb;

ARCHITECTURE behaviour OF integration_tb IS
    SIGNAL s_clk, s_reset : STD_LOGIC := '1';

    SIGNAL s_pc : STD_LOGIC_VECTOR(11 DOWNTO 0);
    SIGNAL s_pc_inc, s_pc_load : STD_LOGIC;
    SIGNAL s_instr : STD_LOGIC_VECTOR(23 DOWNTO 0);
BEGIN
    idecode : ENTITY work.ice_idecode
        PORT MAP(
            imm => s_instr(23),
            grp => s_instr(22 DOWNTO 20),
            opc => s_instr(19 DOWNTO 16),
            pc_inc => s_pc_inc,
            pc_load => s_pc_load
        );

    pc : ENTITY work.ice_pc
        PORT MAP(
            clk => s_clk,
            reset => s_reset,

            inc => s_pc_inc,
            load => s_pc_load,
            imm => s_instr(11 DOWNTO 0),
            pc => s_pc
        );

    imem : ENTITY work.ice_imem
        PORT MAP(
            clk => s_clk,
            reset => s_reset,

            addr => s_pc,
            dout => s_instr
        );

    PROCESS
    BEGIN
        WAIT FOR 500 ps;
        s_reset <= '0';
        s_clk <= NOT s_clk;
    END PROCESS;

END behaviour;