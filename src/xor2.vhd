LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY xor2 IS
    PORT (
        a, b : IN BIT;
        y : OUT BIT);
END xor2;

ARCHITECTURE behaviour OF xor2 IS
BEGIN
    y <= a XOR b;
END behaviour;