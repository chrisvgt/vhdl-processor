LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_idecode_tb IS
END ice_idecode_tb;

ARCHITECTURE behaviour OF ice_idecode_tb IS
    SIGNAL s_imm : STD_LOGIC;
    SIGNAL s_pc_inc, s_pc_load : STD_LOGIC;
    SIGNAL s_grp : STD_LOGIC_VECTOR(2 DOWNTO 0);
    SIGNAL s_opc : STD_LOGIC_VECTOR(3 DOWNTO 0);
BEGIN
    dut : ENTITY work.ice_idecode
        PORT MAP(
            imm => s_imm,
            pc_inc => s_pc_inc,
            pc_load => s_pc_load,
            grp => s_grp,
            opc => s_opc
        );
    PROCESS
    BEGIN
        s_imm <= '0';
        s_grp <= "000";
        s_opc <= "0000";
        WAIT FOR 1 ns; -- expect jump (pc_inc = 1, pc_load = 1)

        s_imm <= '1';
        s_grp <= "000";
        s_opc <= "0000";
        WAIT FOR 1 ns; -- expect jump (pc_inc = 1, pc_load = 1)

        s_imm <= '0';
        s_grp <= "001";
        s_opc <= "0000";
        WAIT FOR 1 ns; -- expect nop (pc_inc = 1, pc_load = 0)

        s_imm <= '0';
        s_grp <= "000";
        s_opc <= "0001";
        WAIT FOR 1 ns; -- expect nop (pc_inc = 1, pc_load = 0)

        s_imm <= '1';
        s_grp <= "111";
        s_opc <= "1111";
        WAIT FOR 1 ns; -- expect nop (pc_inc = 1, pc_load = 0)
    END PROCESS;
END behaviour;