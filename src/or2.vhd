library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity or2 is
    port( a,b : in bit;
            y : out bit);
end or2;

architecture behaviour of or2 is
begin
    y <= a or b;
end behaviour;
