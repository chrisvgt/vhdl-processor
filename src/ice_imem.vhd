LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_imem IS
    GENERIC (instr_width : POSITIVE := 24);
    PORT (
        clk : IN STD_LOGIC;
        reset : IN STD_LOGIC;
        addr : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
        dout : OUT STD_LOGIC_VECTOR(instr_width - 1 DOWNTO 0));
END ice_imem;