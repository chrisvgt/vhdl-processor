LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_shifter IS
    GENERIC (
        width : POSITIVE := 16;
        width2 : POSITIVE := 4);
    PORT (
        a : IN STD_LOGIC_VECTOR(width - 1 DOWNTO 0);
        pos, opc : IN STD_LOGIC_VECTOR(width2 - 1 DOWNTO 0);
        y : OUT STD_LOGIC_VECTOR(width - 1 DOWNTO 0));
END ice_shifter;

ARCHITECTURE behaviour OF ice_shifter IS

BEGIN
    PROCESS (a, pos, opc)
        VARIABLE t1, t2, t3 : STD_LOGIC_VECTOR (15 DOWNTO 0);
    BEGIN
        IF opc = "0000" THEN -- LEFT SHIFT
            IF pos(0) = '1' THEN
                t1 := a(width - 2 DOWNTO 0) & '0';
            ELSE
                t1 := a;
            END IF;

            IF pos(1) = '1' THEN
                t2 := t1(width - 3 DOWNTO 0) & "00";
            ELSE
                t2 := t1;
            END IF;

            IF pos(2) = '1' THEN
                t3 := t2(width - 5 DOWNTO 0) & "0000";
            ELSE
                t3 := t2;
            END IF;

            IF pos(3) = '1' THEN
                y <= a(width - 9 DOWNTO 0) & "00000000";
            ELSE
                y <= t3;
            END IF;

        ELSIF opc = "0001" THEN -- RIGHT SHIFT
            IF pos(0) = '1' THEN
                t1 := '0' & a(width - 1 DOWNTO 1);
            ELSE
                t1 := a;
            END IF;

            IF pos(1) = '1' THEN
                t2 := "00" & t1(width - 1 DOWNTO 2);
            ELSE
                t2 := t1;
            END IF;

            IF pos(2) = '1' THEN
                t3 := "0000" & t2(width - 1 DOWNTO 4);
            ELSE
                t3 := t2;
            END IF;

            IF pos(3) = '1' THEN
                y <= "00000000" & t1(width - 1 DOWNTO 8);
            ELSE
                y <= t3;
            END IF;

        END IF;
    END PROCESS;
END behaviour;