LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY ice_alu IS
    GENERIC (width : POSITIVE := 16);
    PORT (
        a, b : IN STD_LOGIC_VECTOR(width - 1 DOWNTO 0);
        y : OUT STD_LOGIC_VECTOR(width - 1 DOWNTO 0);
        opc : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
        abl : IN STD_LOGIC);
END ice_alu;

ARCHITECTURE behaviour OF ice_alu IS
BEGIN
    y <= a AND b WHEN opc = "0000" ELSE
        a OR b WHEN opc = "0001" ELSE
        a NAND b WHEN opc = "0010" ELSE
        a NOR b WHEN opc = "0011" ELSE
        a XOR b WHEN opc = "0100" ELSE
        a XNOR b WHEN opc = "0101" ELSE
        NOT a WHEN opc = "0111" ELSE
        (OTHERS => '0');
END behaviour;