LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY neq4 IS
    GENERIC (width : POSITIVE := 4);
    PORT (
        a, b : IN bit_vector(width - 1 DOWNTO 0);
        y : OUT BIT);

END neq4;

-- ANTIVALENZ GATTER ^= XOR GATTER
ARCHITECTURE behaviour OF neq4 IS
BEGIN
    PROCESS (A, B)
    BEGIN
        IF ((a XOR b) = "0000") THEN
            Y <= '0';
        ELSE
            Y <= '1';
        END IF;
    END PROCESS;
END behaviour;

ARCHITECTURE dataflow OF neq4 IS
BEGIN
    Y <= '0' WHEN (a XOR b) = "0000" ELSE
        '1';
END dataflow;

ARCHITECTURE logic OF neq4 IS
BEGIN
    Y <= (a(0) XOR b(0)) OR (a(1) XOR b(1)) OR (a(2) XOR b(2)) OR (a(3) XOR b(3));
END logic;

ARCHITECTURE netlist OF neq4 IS
    SIGNAL xor_0_out, xor_1_out, xor_2_out, xor_3_out, or_1_out, or_2_out : BIT;
BEGIN
    xor_0 : ENTITY work.xor2(behaviour) PORT MAP (a => a(0), b => b(0), y => xor_0_out);
    xor_1 : ENTITY work.xor2(behaviour) PORT MAP (a => a(1), b => b(1), y => xor_1_out);
    xor_2 : ENTITY work.xor2(behaviour) PORT MAP (a => a(2), b => b(2), y => xor_2_out);
    xor_3 : ENTITY work.xor2(behaviour) PORT MAP (a => a(3), b => b(3), y => xor_3_out);

    or_1 : ENTITY work.or2(behaviour) PORT MAP (a => xor_0_out, b => xor_1_out, y => or_1_out);
    or_2 : ENTITY work.or2(behaviour) PORT MAP (a => xor_2_out, b => xor_3_out, y => or_2_out);

    or_0 : ENTITY work.or2(behaviour) PORT MAP (a => or_1_out, b => or_2_out, y => y);
END netlist;